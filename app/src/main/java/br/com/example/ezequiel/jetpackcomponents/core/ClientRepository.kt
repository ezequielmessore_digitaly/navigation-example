package br.com.example.ezequiel.jetpackcomponents.core

import android.content.Context
import br.com.example.ezequiel.jetpackcomponents.client.model.Client

class ClientRepository {

    fun getClients(context: Context?) =
        context?.let { TestSharedPreferences.CLIENTS.getObjects<Client>(context) } ?: listOf()



    fun addClient(context: Context, vararg clients: Client) {
        TestSharedPreferences.CLIENTS.putObjects(context, clients.toList())
    }

}