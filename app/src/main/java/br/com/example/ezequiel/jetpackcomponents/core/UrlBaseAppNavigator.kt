package br.com.example.ezequiel.jetpackcomponents.core

import android.app.Activity
import android.content.Intent
import android.net.Uri

object UrlBaseAppNavigator {

    fun registerUser(context: Activity) {
        "https://example.test/register".toUri().startDeepLink(context)
    }

    private fun Uri.startDeepLink(context: Activity) {
        Intent(Intent.ACTION_VIEW, this).apply {
            addCategory(Intent.CATEGORY_BROWSABLE)
            `package` = context.packageName
            context.startActivity(this)
        }
    }
}

fun String.toUri(): Uri = Uri.parse(this)