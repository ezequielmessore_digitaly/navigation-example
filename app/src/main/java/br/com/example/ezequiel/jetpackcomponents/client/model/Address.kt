package br.com.example.ezequiel.jetpackcomponents.client.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Address(var street: String, var neighborhood: String, var number: Int): Parcelable