package br.com.example.ezequiel.jetpackcomponents.client.register

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.example.ezequiel.jetpackcomponents.R
import br.com.example.ezequiel.jetpackcomponents.client.model.Address
import br.com.example.ezequiel.jetpackcomponents.core.ClientRepository
import br.com.example.ezequiel.jetpackcomponents.databinding.FragmentRegisterAddressClientBinding
import kotlinx.android.synthetic.main.fragment_register_address_client.*

class RegisterClientAddressFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentRegisterAddressClientBinding>(
            inflater,
            R.layout.fragment_register_address_client,
            container,
            false
        )

        val client = RegisterClientAddressFragmentArgs.fromBundle(arguments!!).client
        binding.address = client.address

        binding.fab.setOnClickListener {
            client.address = Address(et_street.text.toString(), et_neighborhood.text.toString(), et_number.text.toString().toInt())

            ClientRepository().addClient(context!!, client)
            activity?.finish()
        }

        return binding.root
    }

}