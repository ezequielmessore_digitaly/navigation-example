package br.com.example.ezequiel.jetpackcomponents.core

import android.content.Context
import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

enum class TestSharedPreferences constructor(private val key: String) {

    CLIENTS("CLIENTS"),
    ;

    private fun getSharedPreferences(context: Context) =
        context.getSharedPreferences("DRAWS_SHARED_PREFERENCES", Context.MODE_PRIVATE)


    fun putBoolean(context: Context, data: Boolean) {
        getSharedPreferences(context).edit().putBoolean(this.key, data).apply()
    }

    fun getBoolean(context: Context): Boolean {
        return getSharedPreferences(context).getBoolean(this.key, false)
    }

    fun putObjects(context: Context, data: List<Parcelable>) {
        val list = arrayListOf<Parcelable>()
        getObjects<Parcelable>(context)?.let {
            list.addAll(it)
        }
        list.addAll(data)
        put(context, Gson().toJson(list))
    }

    fun <T> getObjects(context: Context): List<T> {
        val list = get(context)
        return if (list.isNotEmpty()) {
            val fromJson = Gson().fromJson<List<T>>(list)
            println(fromJson[0])
            fromJson
        } else listOf()
    }


    fun put(context: Context, data: String) {
        getSharedPreferences(context).edit().putString(this.key, data).apply()
    }

    fun get(context: Context): String {
        return getSharedPreferences(context).getString(this.key, "") ?: ""
    }

    fun getInt(context: Context): Int {
        return getSharedPreferences(context).getInt(this.key, 0)
    }

    fun putInt(context: Context, value: Int) {
        getSharedPreferences(context).edit().putInt(this.key, value).apply()
    }

    fun remove(context: Context) {
        getSharedPreferences(context).edit().remove(this.key).apply()
    }

    fun clear(context: Context) {
        getSharedPreferences(context).edit().clear().apply()
    }

    private inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)
}