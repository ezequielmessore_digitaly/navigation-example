package br.com.example.ezequiel.jetpackcomponents.client

import android.os.Bundle
import br.com.example.ezequiel.jetpackcomponents.core.BaseActivity
import br.com.example.ezequiel.jetpackcomponents.R

class ClientActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client)
    }

}