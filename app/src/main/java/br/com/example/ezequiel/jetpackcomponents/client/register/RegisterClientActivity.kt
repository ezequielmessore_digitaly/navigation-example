package br.com.example.ezequiel.jetpackcomponents.client.register

import android.os.Bundle
import br.com.example.ezequiel.jetpackcomponents.R
import br.com.example.ezequiel.jetpackcomponents.core.BaseActivity

class RegisterClientActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_client)
    }
}