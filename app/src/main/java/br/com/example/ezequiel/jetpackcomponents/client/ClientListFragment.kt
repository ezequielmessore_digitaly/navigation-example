package br.com.example.ezequiel.jetpackcomponents.client

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.example.ezequiel.jetpackcomponents.R
import br.com.example.ezequiel.jetpackcomponents.core.UrlBaseAppNavigator
import br.com.example.ezequiel.jetpackcomponents.core.ClientRepository
import br.com.example.ezequiel.jetpackcomponents.databinding.FragmentListClientBinding

class ClientListFragment : Fragment() {

    private lateinit var binding: FragmentListClientBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_client, container, false)

        binding.fab.setOnClickListener {
            activity?.let {
                UrlBaseAppNavigator.registerUser(it)
            }
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        val clients = ClientRepository().getClients(context)
        val toString = clients.toString()
        binding.tvClients.text = toString
        println(toString)
    }
}