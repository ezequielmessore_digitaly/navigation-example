package br.com.example.ezequiel.jetpackcomponents.client.register

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import br.com.example.ezequiel.jetpackcomponents.R
import br.com.example.ezequiel.jetpackcomponents.client.model.Client
import br.com.example.ezequiel.jetpackcomponents.databinding.FragmentRegisterClientBinding
import kotlinx.android.synthetic.main.fragment_register_client.*

class RegisterClientFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentRegisterClientBinding>(inflater, R.layout.fragment_register_client, container, false)


        binding.fab.setOnClickListener {
            val client = Client(et_name.text.toString(), et_age.text.toString().toInt())

            val action = RegisterClientFragmentDirections.actionClientToAddress(client)
            NavHostFragment.findNavController(this).navigate(action)
        }

        return binding.root
    }

}