package br.com.example.ezequiel.jetpackcomponents.client.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Client(var name: String, var age: Int) : Parcelable {
    var address: Address? = null
}